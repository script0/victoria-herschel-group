//Install express server
const express = require("express");
const path = require("path");
const os = require("os")
const bodyParser = require("body-parser");
const nodemailer = require("nodemailer");
const { google } = require("googleapis");

var cors = require('cors')

const multer  = require('multer');
const upload = multer({ dest: os.tmpdir() });

const { MongoClient, ObjectId } = require('mongodb');
const url = 'mongodb+srv://script0:script0@cluster0.0soh0.mongodb.net/endigraphe?retryWrites=true&w=majority';
const client = new MongoClient(url);
const dbName = 'endigraphe';
client.connect();
console.log('Connected successfully to DB');
const db = client.db(dbName);


const app = express();

// Serve only the static files form the dist directory
app.use(express.static(path.join(__dirname, "/dist/victoria-herschel-group/")));
app.use(bodyParser.json());

const transport = nodemailer.createTransport({
  host : "mx-dc03.ewodi.net",
  auth : {
    user : "support@victoriaherschelgroup.cm",
    pass : "eg_szB2XMHv?"
  }
});

const rhTransport = nodemailer.createTransport({
  host : "mx-dc03.ewodi.net",
  auth : {
    user : "rh@victoriaherschelgroup.cm",
    pass : "Md_zeJRc1#6s"
  }
});

app.post("/sendemail", function (req, res) {
  //console.log("request body:", req.body)
  const mailOptions = {
    from: "support@victoriaherschelgroup.cm", // sender
    to: req.body.to, // receiver
    subject: "VICTORIA HERSCHEL GROUP - Test", // Subject
    html: req.body.content, // html body
  };
  transport.sendMail(mailOptions, function (err, result) {
    if (err) {
      res.send({
        message: err,
      });
    } else {
      transport.close();
      res.send({
        message: "Email has been sent: check your inbox!",
      });
    }
  });
});

app.post("/candidature/send", upload.single('file'), cors(), function (req, res) {
  console.log("request body:", req.body)
  console.log("request file:", req.file)

  const mailOptions = {
    from: "support@victoriaherschelgroup.cm", // sender
    to: "rh@victoriaherschelgroup.cm", // receiver
    //to: "bekolleisaac@gmail.com",
    subject: "Candidature Spontanée", // Subject
    html: "Bonjour M./Mme <br> <br> Je suis <strong>"+req.body.name+"</strong> \n \
            <ul> \n \
              <li> Email : <strong>" + req.body.email + "</strong> </li> \n \
              <li> Tel : <strong> " + req.body.tel + "</strong> </li> \n \
              <li> Genre : <strong> " + req.body.genre + "</strong> </li> \n \
              <li> Birthdate : <strong> " + req.body.birthdate + "</strong> </li> \n \
              <li> Address : <strong> " + req.body.address + "</strong> </li> \n \
              <li> CV : <strong> ci joint </strong> </li> \n \
            </ul> \n \
          Je viens par mail déposer ma candidature spontanée. \n Bien à vous.",
    attachments: [
        {
            filename: req.file.originalname, // <= Here: made sure file name match
            path: req.file.path, // <= Here
            contentType: 'application/pdf'
        }
    ]
  };

  transport.sendMail(mailOptions, function (err, result) {
    if (err) {
      res.status(500).send({
        err: err,
      });
    } else {
      transport.close();
      const feedback = {
        from: "rh@victoriaherschelgroup.cm", // sender
        to: req.body.email, // receiver
        //to: "bekolleisaac@gmail.com",
        subject: "RH - VICTORIA HERSCHEL GROUP - Candidature Recue", // Subject
        html: "Bonjour <strong>" + req.body.genre + " " + req.body.name + "</strong> <br> Nous avons recu votre candidature spontannée et \
        l'étudions de suite. Nous vous recontacterons une fois que nous trouverons le meilleur poste correspondant à votre background. <br> \
        <br> \
        Nous vous remercions pour votre intérêt envers notre structure et espérons que feriez bientot partie des notres. \
        <br> <br> \
        Bien à vous .\
        <br><br> \
        <strong> RH - Victoria Herschel Group </strong>"
      }
      rhTransport.sendMail(feedback, function (err, result) {
        if (err) {
          res.status(500).send({
            err: err,
          });
        } else {
          rhTransport.close();
          res.status(200).send({
            message: "Email has been sent: check your inbox!",
          });
        }
      })
    }
  });
});

app.post("/signup",  async function (req, res) {
  let userCollection = db.collection('users')
  let user = req.body
  user["patients"] = []
  let newUser = await userCollection.insertOne(user)
  //console.log("user:", newUser)
  res.send(req.body.user)
});


app.post("/signin",  async function (req, res) {
  let userCollection = db.collection('users')
  let user = await userCollection.find({password:req.body.password, username:req.body.username}).toArray()
  if(user.length){
    res.send({user: user[0]})
  }else{
    res.send({user: null})
  }
});

app.post("/add-patient/:id",  async function (req, res) {
  let patientCollection = db.collection('users') 
  let user = await patientCollection.find({_id:ObjectId(req.params.id)}).toArray()
  console.log("user:", user[0])
  user[0].patients.push(req.body)

  console.log("user:", user[0])
  let newPatient = await patientCollection.updateOne({_id:ObjectId(req.params.id)},{$set:{patients:user[0].patients}})
  console.log("newPatient:", newPatient)
  res.send(req.body)
});

app.get("/*", function (req, res) {
  res.sendFile(path.join(__dirname, "/dist/victoria-herschel-group/index.html"));
});

// Start the app by listening on the default Heroku port
console.log("App running on port ", process.env.PORT || 8080)
app.listen(process.env.PORT || 8080);
