import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app.routing';
import { AppComponent } from './app.component';
import { SignupComponent } from './signup/signup.component';
import { LandingComponent } from './landing/landing.component';
import { ProfileComponent } from './profile/profile.component';
import { DocsComponent } from './docs/docs.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { FooterComponent } from './shared/footer/footer.component';
import { LoginComponent } from './login/login.component';
import { CommonModule } from '@angular/common';
import { AlertsSectionComponent } from './sections/alerts-section/alerts-section.component';
import { AngularSectionComponent } from './sections/angular-section/angular-section.component';
import { ButtonsSectionComponent } from './sections/buttons-section/buttons-section.component';
import { CrsSectionComponent } from './sections/crs-section/crs-section.component';
import { InputsSectionComponent } from './sections/inputs-section/inputs-section.component';
import { NgbdModalComponent, NgbdModalContent } from './sections/modal/modal.component';
import { NavigationSectionComponent } from './sections/navigation-section/navigation-section.component';
import { NucleoSectionComponent } from './sections/nucleo-section/nucleo-section.component';
import { SectionsComponent } from './sections/sections.component';
import { TabsSectionComponent } from './sections/tabs-section/tabs-section.component';
import { TypographySectionComponent } from './sections/typography-section/typography-section.component';
import { VersionsSectionComponent } from './sections/versions-section/versions-section.component';
import { NouisliderModule } from 'ng2-nouislider';
import { JwBootstrapSwitchNg2Module } from 'jw-bootstrap-switch-ng2';
import { ContactComponent } from './contact/contact.component';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { CandidatureSpontaneeComponent } from './candidature-spontanee/candidature-spontanee.component';
import { SwiperModule } from "swiper/angular";
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    SignupComponent,
    LandingComponent,
    ProfileComponent,
    NavbarComponent,
    FooterComponent,
    LoginComponent,
    DocsComponent,
    SectionsComponent,
    ButtonsSectionComponent,
    InputsSectionComponent,
    CrsSectionComponent,
    NavigationSectionComponent,
    TabsSectionComponent,
    AlertsSectionComponent,
    TypographySectionComponent,
    AngularSectionComponent,
    NucleoSectionComponent,
    VersionsSectionComponent,
    NgbdModalComponent,
    NgbdModalContent,
    ContactComponent,
    CandidatureSpontaneeComponent
  ],
  imports: [
    BrowserModule,
    NgbModule,
    RouterModule,
    AppRoutingModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NouisliderModule,
    JwBootstrapSwitchNg2Module,
    LeafletModule,
    SwiperModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
