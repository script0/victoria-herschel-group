export const PARTENAIRES = [
    {
      link : 'http://www.areaassurance.com/',
      img : './assets/img/brand/area.png',
      name: 'AREA Assurance'
    },
    {
      link : 'https://prubeneficial.cm',
      img : './assets/img/brand/beneficial.png',
      name: 'Prudencial Beneficial Alliance'
    },
    {
      link : 'https://www.gmcassurances.com/',
      img : './assets/img/brand/gmc.png',
      name: 'GMC Assurances'
    },
    {
      link : 'https://www.royalonyx.cm/',
      img : './assets/img/brand/royal.png',
      name: 'Royal Onyx'
    },
    {
      link : 'https://cm.sanlam.com/',
      img : './assets/img/brand/salam.png',
      name: 'Sanlam'
    }
]