import { Component, OnInit, Type } from "@angular/core";
import { Router, NavigationEnd, NavigationStart } from "@angular/router";
import { Location, PopStateEvent } from "@angular/common";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { CandidatureSpontaneeComponent } from "src/app/candidature-spontanee/candidature-spontanee.component";
import { PARTENAIRES } from "../partenaires";

@Component({
    selector: "app-navbar",
    templateUrl: "./navbar.component.html",
    styleUrls: ["./navbar.component.scss"],
})
export class NavbarComponent implements OnInit {
    public isCollapsed = true;
    private lastPoppedUrl: string;
    private yScrollStack: number[] = [];

    partenaires = PARTENAIRES;

    MODALS: { [name: string]: { component: Type<any>; options: any } } = {
        candidatureSpontanee: {
            component: CandidatureSpontaneeComponent,
            options: { centered: true },
        },
    };


    constructor(
        public location: Location,
        private router: Router,
        private _modalService: NgbModal
    ) { }

    ngOnInit() {
        this.router.events.subscribe((event) => {
            this.isCollapsed = true;
            if (event instanceof NavigationStart) {
                if (event.url != this.lastPoppedUrl)
                    this.yScrollStack.push(window.scrollY);
            } else if (event instanceof NavigationEnd) {
                if (event.url == this.lastPoppedUrl) {
                    this.lastPoppedUrl = undefined;
                    window.scrollTo(0, this.yScrollStack.pop());
                } else window.scrollTo(0, 0);
            }
        });
        this.location.subscribe((ev: PopStateEvent) => {
            this.lastPoppedUrl = ev.url;
        });
    }

    isHome() {
        var titlee = this.location.prepareExternalUrl(this.location.path());

        if (titlee === "#/home") {
            return true;
        } else {
            return false;
        }
    }
    isDocumentation() {
        var titlee = this.location.prepareExternalUrl(this.location.path());
        if (titlee === "#/documentation") {
            return true;
        } else {
            return false;
        }
    }

    openModal(name: string) {
        this._modalService.open(
            this.MODALS[name].component,
            this.MODALS[name].options
        );
    }
}
