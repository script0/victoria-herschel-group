import { ChangeDetectorRef, Component, HostListener, NgZone, OnInit, ViewChild } from '@angular/core';
import { SwiperComponent } from "swiper/angular";
import Swiper from "swiper/types/swiper-class";
// import Swiper core and required components
import SwiperCore , {
  Navigation,
  Pagination,
  Scrollbar,
  A11y,
  Virtual,
  Zoom,
  Autoplay,
  Thumbs,
  Controller,
} from 'swiper';
import { PARTENAIRES } from '../shared/partenaires';
// install Swiper components
SwiperCore.use([
  Navigation,
  Pagination,
  Scrollbar,
  A11y,
  Virtual,
  Zoom,
  Autoplay,
  Thumbs,
  Controller
]);


@Component({
    selector: 'app-landing',
    templateUrl: './landing.component.html',
    styleUrls: ['./landing.component.scss']
})

export class LandingComponent implements OnInit {
  focus: any;
  focus1: any;
  focus2: any;

  partenairePerView: number = 3

  @ViewChild('swiperRef', { static: false }) swiperRef?: SwiperComponent;


  partenaires = PARTENAIRES
  public innerWidth: any;

  constructor(private cd: ChangeDetectorRef, private ngZone: NgZone) { }

  ngOnInit() {
    this.innerWidth = window.innerWidth;
    this.updatePaternairePerView()

  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
      this.innerWidth = window.innerWidth;
      this.updatePaternairePerView()
  }

  updatePaternairePerView(){
    if(this.innerWidth < 742){
      this.partenairePerView = 1
    }
    else if(this.innerWidth < 930){
      this.partenairePerView = 2
    }else{
      this.partenairePerView = 3
    }
  }

}
