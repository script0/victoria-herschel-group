import { Component } from '@angular/core';
import { circle, icon, latLng, marker, polygon, tileLayer } from 'leaflet';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent {

  focus: any;
  focus1: any;
  focus2: any;

  options = {
    layers: [
      tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { maxZoom: 18, attribution: '...' })
    ],
    zoom: 18,
    center: latLng(3.8841277, 11.5353122)
  };

  layersControl = {
    baseLayers: {
      'Open Street Map': tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { maxZoom: 18, attribution: '...' }),
      'Open Cycle Map': tileLayer('https://{s}.tile.opencyclemap.org/cycle/{z}/{x}/{y}.png', { maxZoom: 18, attribution: '...' })
    },
    overlays: {}
  }

  icon = {
    icon: icon({
      iconSize: [ 25, 41 ],
      iconAnchor: [ 13, 0 ],
      // specify the path here
      iconUrl: './assets/img/leaflet/marker-icon.png',
      shadowUrl: './assets/img/leaflet/marker-shadow.png'
   })
  };

  layers = [
    marker([ 3.8841277, 11.5353122], this.icon).bindPopup('<strong> VICTORIA HERSCHEL GROUP </strong> <br> Immeuble Victoria Herschel Group, <br> Rue NGOUSSO BP 6726, Yaoundé. <br> Tel: +237 690 557 130 / +237 681 384 514 ', {autoClose:false}).openPopup()
  ];


}
