import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CandidatureSpontanee } from 'src/app/interface/candidature-spontanee';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EmailService {

  href = environment.backendLink+"candidature/send";

  constructor(private _httpClient: HttpClient) {}
  
  sendCandidatureSpontanee = (candidature : CandidatureSpontanee) : Observable<any> =>{
      candidature.genre = candidature.genre =="H" ? "M." : "Mme" 

      const form = new FormData();
      form.append('name', candidature.name);
      form.append('birthdate', candidature.birthdate);
      form.append('genre', candidature.genre);
      form.append('tel', candidature.tel);
      form.append('email', candidature.email);
      form.append('address', candidature.address);
      form.append('file', candidature.cv);

      return this._httpClient.post(this.href,form)
  }

}
