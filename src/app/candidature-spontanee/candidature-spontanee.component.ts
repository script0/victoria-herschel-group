import { Component,  Inject, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormControl, FormGroup } from '@angular/forms';
import { EmailService } from '../services/email/email.service';

@Component({
  selector: 'app-candidature-spontanee',
  templateUrl: './candidature-spontanee.component.html',
  styleUrls: ['./candidature-spontanee.component.css']
})
export class CandidatureSpontaneeComponent implements OnInit , AfterViewInit{
	constructor(public modal: NgbActiveModal, private _emailService : EmailService) {}
  candidatureForm = new FormGroup({
    name : new FormControl(''),
    birthdate : new FormControl(''),
    genre : new FormControl(''),
    tel : new FormControl(''),
    email : new FormControl(''),
    address : new FormControl(''),
  })

  candidatureFieldsFocus = {
    name : false,
    birthdate : false,
    genre: false,
    tel: false,
    email: false,
    address: false,
    cv: false
  }

  CV_MAX_SIZE = 8000000 // 8Mo

  @ViewChild("cv", {static: true}) cv: any;
  isWorking : boolean = true;
  isFormInvalid: boolean = false
  isCVTooHeavy: boolean = false

  submit():void{
    this.isWorking = true;
    this.isFormInvalid = false
    this.isCVTooHeavy = false

    if(this.cv.nativeElement.files.length==0){
      this.isFormInvalid=true
    }else if(this.cv.nativeElement.files[0].size > this.CV_MAX_SIZE ){
      this.isCVTooHeavy = true
      
    }else{
      console.log("Candidature spontanée:",this.candidatureForm.value, ", cv : ",this.cv.nativeElement.files[0])
      this._emailService.sendCandidatureSpontanee({
        ...this.candidatureForm.value,
        cv : this.cv.nativeElement.files[0]
      }).subscribe(res=>{
        if(res.err){
          console.log("Candidature spontanée Failed. Error : ", res.err)
        }else{
          this.modal.close('Ok click');
        }
      })
    }
    this.isWorking = false;
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    this.isWorking = false;
  }
}