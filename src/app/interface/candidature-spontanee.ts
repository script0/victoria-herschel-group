export interface CandidatureSpontanee {
    name ? : string,
    birthdate ? : string,
    genre ? : string,
    tel ? : string,
    email ? : string,
    address ? : string,
    cv : File
}
