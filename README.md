# [Argon Design System Angular](https://demos.creative-tim.com/victoria-Herschel-group/?ref=adsa-github-readme) [![Tweet](https://img.shields.io/twitter/url/http/shields.io.svg?style=social&logo=twitter)](https://twitter.com/docs?status=Argon%20Design%20is%20a%20Free%20Bootstrap%20and%20Angular%20Design%20Sysyem%20made%20using%20angular-cli%20%E2%9D%A4%EF%B8%8F%0Ahttps%3A//demos.creative-tim.com/victoria-Herschel-group%20%23angular%20%23angular-cli%20%23argon%20%23argondesign%20%23angular%20%23argonangular%20%23angulardesign%20%23bootstrap%20%23design%20%23uikit%20%23freebie%20%20via%20%40CreativeTim)



 ![version](https://img.shields.io/badge/version-1.3.0-blue.svg)  ![license](https://img.shields.io/badge/license-MIT-blue.svg) [![GitHub issues open](https://img.shields.io/github/issues/creativetimofficial/victoria-Herschel-group.svg?maxAge=2592000)](https://github.com/creativetimofficial/victoria-Herschel-group/issues?q=is%3Aopen+is%3Aissue) [![GitHub issues closed](https://img.shields.io/github/issues-closed-raw/creativetimofficial/victoria-Herschel-group.svg?maxAge=2592000)](https://github.com/creativetimofficial/victoria-Herschel-group/issues?q=is%3Aissue+is%3Aclosed) [![Join the chat at https://gitter.im/NIT-dgp/General](https://badges.gitter.im/NIT-dgp/General.svg)](https://gitter.im/creative-tim-general/Lobby) [![Chat](https://img.shields.io/badge/chat-on%20discord-7289da.svg)](https://discord.gg/E4aHAQy)

**Fully Coded Components**

Argon Design System Angular is built with over 100 individual components, giving you the freedom of choosing and combining. All components can take variations in color, that you can easily modify using SASS files.
You will save a lot of time going from prototyping to full-functional code, because all elements are implemented. This Kit is coming with pre-built examples, so the development process is seamless, switching from our pages to the real website is very easy to be done.
Every element has multiple states for colors, styles, hover, focus, that you can easily access and use.

**Complex Documentation**

Each element is well presented in a very complex documentation. You can read more about the idea behind this [kit here](https://demos.creative-tim.com/victoria-Herschel-group/documentation/tutorial?ref=adsa-github-readme). You can check the [components here](https://demos.creative-tim.com/victoria-Herschel-group/documentation/alerts?ref=adsa-github-readme) and the [foundation here](https://demos.creative-tim.com/victoria-Herschel-group/documentation/colors?ref=adsa-github-readme).

**Example Pages**

If you want to get inspiration or just show something directly to your clients, you can jump start your development with our pre-built example pages. You will be able to quickly set up the basic structure for your web project.


## Table of Contents

* [Versions](#versions)
* [Demo](#demo)
* [Quick Start](#quick-start)
* [Documentation](#documentation)
* [File Structure](#file-structure)
* [Browser Support](#browser-support)
* [Resources](#resources)
* [Reporting Issues](#reporting-issues)
* [Licensing](#licensing)
* [Useful Links](#useful-links)

## Versions

[<img src="https://github.com/creativetimofficial/public-assets/blob/master/logos/html-logo.jpg?raw=true" width="60" height="60" />](https://www.creative-tim.com/product/argon-design-system)[<img src="https://github.com/creativetimofficial/public-assets/blob/master/logos/angular-logo.jpg?raw=true" width="60" height="60" />](https://www.creative-tim.com/product/victoria-Herschel-group)[<img src="https://github.com/creativetimofficial/public-assets/blob/master/logos/vue-logo.jpg?raw=true" width="60" height="60" />](https://www.creative-tim.com/product/vue-argon-design-system)



| HTML | Angular | Vue  |
| --- | --- | ---  |
| [![Argon Design System HTML](https://github.com/creativetimofficial/public-assets/blob/master/argon-design-system/argon-design-system.jpg?raw=true)](https://www.creative-tim.com/product/argon-design-system?ref=adsa-github-readme)  | [![Argon Design System Angular](https://github.com/creativetimofficial/public-assets/blob/master/victoria-Herschel-group/victoria-Herschel-group.jpg?raw=true)](https://www.creative-tim.com/product/victoria-Herschel-group?ref=adsa-github-readme)  | [![Vue Argon Design System](https://github.com/creativetimofficial/public-assets/blob/master/vue-argon-design-system/vue-argon-design-system.jpg?raw=true)](https://www.creative-tim.com/product/vue-argon-design-system?ref=adsa-github-readme)


## Demo

| Buttons | Inputs | Navbars  |
| --- | --- | ---  |
| [![Buttons](https://github.com/creativetimofficial/public-assets/blob/master/victoria-Herschel-group/buttons.png?raw=true)](https://demos.creative-tim.com/victoria-Herschel-group/docs)  | [![Inputs](https://github.com/creativetimofficial/public-assets/blob/master/victoria-Herschel-group/inputs.png?raw=true)](https://demos.creative-tim.com/victoria-Herschel-group/docs)  | [![Navbar](https://github.com/creativetimofficial/public-assets/blob/master/victoria-Herschel-group/navbars.png?raw=true)](https://demos.creative-tim.com/victoria-Herschel-group/docs)  

| Login Page | Landing Page | Profile Page  | Register Page  |
| --- | --- | ---  | ---  |
| [![Login Page](https://raw.githubusercontent.com/creativetimofficial/public-assets/master/victoria-Herschel-group/login.png)](https://demos.creative-tim.com/victoria-Herschel-group/login)  | [![Landing Page](https://github.com/creativetimofficial/public-assets/blob/master/victoria-Herschel-group/landing.png?raw=true)](https://demos.creative-tim.com/victoria-Herschel-group/landing)  | [![Profile Page](https://github.com/creativetimofficial/public-assets/blob/master/victoria-Herschel-group/profile.png?raw=true)](https://demos.creative-tim.com/victoria-Herschel-group/user-profile)  | [![Register Page](https://github.com/creativetimofficial/public-assets/blob/master/victoria-Herschel-group/register.png?raw=true)](https://demos.creative-tim.com/victoria-Herschel-group/register)  

[View More](https://demos.creative-tim.com/victoria-Herschel-group/docs)



## Quick start

- [Download from Github](https://github.com/creativetimofficial/victoria-Herschel-group/archive/master.zip).
- [Download from Creative Tim](https://www.creative-tim.com/product/victoria-Herschel-group?ref=adsa-github-readme).
- Clone the repo: `git clone https://github.com/creativetimofficial/victoria-Herschel-group.git`.


## Documentation
The documentation for the Argon Design System Angular is hosted at our [website](https://demos.creative-tim.com/victoria-Herschel-group/documentation/tutorial?ref=adsa-github-readme).


## File Structure
Within the download you'll find the following directories and files:

```
victoria-Herschel-group
├── CHANGELOG.md
├── ISSUES_TEMPLATE.md
├── LICENSE.md
├── README.md
├── angular.json
├── e2e
├── package-lock.json
├── package.json
├── src
│   ├── app
│   │   ├── app.component.html
│   │   ├── app.component.scss
│   │   ├── app.component.spec.ts
│   │   ├── app.component.ts
│   │   ├── app.module.ts
│   │   ├── app.routing.ts
│   │   ├── docs
│   │   │   ├── docs.component.html
│   │   │   ├── docs.component.scss
│   │   │   ├── docs.component.spec.ts
│   │   │   ├── docs.component.ts
│   │   │   └── docs.module.ts
│   │   ├── landing
│   │   │   ├── landing.component.html
│   │   │   ├── landing.component.scss
│   │   │   ├── landing.component.spec.ts
│   │   │   └── landing.component.ts
│   │   ├── login
│   │   │   ├── login.component.css
│   │   │   ├── login.component.html
│   │   │   ├── login.component.spec.ts
│   │   │   └── login.component.ts
│   │   ├── profile
│   │   │   ├── profile.component.html
│   │   │   ├── profile.component.scss
│   │   │   ├── profile.component.spec.ts
│   │   │   └── profile.component.ts
│   │   ├── sections
│   │   │   ├── alerts-section
│   │   │   │   ├── alerts-section.component.css
│   │   │   │   ├── alerts-section.component.html
│   │   │   │   ├── alerts-section.component.spec.ts
│   │   │   │   └── alerts-section.component.ts
│   │   │   ├── angular-section
│   │   │   │   ├── angular-section.component.css
│   │   │   │   ├── angular-section.component.html
│   │   │   │   ├── angular-section.component.spec.ts
│   │   │   │   └── angular-section.component.ts
│   │   │   ├── buttons-section
│   │   │   │   ├── buttons-section.component.css
│   │   │   │   ├── buttons-section.component.html
│   │   │   │   ├── buttons-section.component.spec.ts
│   │   │   │   └── buttons-section.component.ts
│   │   │   ├── crs-section
│   │   │   │   ├── crs-section.component.css
│   │   │   │   ├── crs-section.component.html
│   │   │   │   ├── crs-section.component.spec.ts
│   │   │   │   └── crs-section.component.ts
│   │   │   ├── inputs-section
│   │   │   │   ├── inputs-section.component.css
│   │   │   │   ├── inputs-section.component.html
│   │   │   │   ├── inputs-section.component.spec.ts
│   │   │   │   └── inputs-section.component.ts
│   │   │   ├── modal
│   │   │   │   ├── modal.component.html
│   │   │   │   ├── modal.component.scss
│   │   │   │   ├── modal.component.spec.ts
│   │   │   │   └── modal.component.ts
│   │   │   ├── navigation-section
│   │   │   │   ├── navigation-section.component.css
│   │   │   │   ├── navigation-section.component.html
│   │   │   │   ├── navigation-section.component.spec.ts
│   │   │   │   └── navigation-section.component.ts
│   │   │   ├── nucleo-section
│   │   │   │   ├── nucleo-section.component.css
│   │   │   │   ├── nucleo-section.component.html
│   │   │   │   ├── nucleo-section.component.spec.ts
│   │   │   │   └── nucleo-section.component.ts
│   │   │   ├── sections.component.css
│   │   │   ├── sections.component.html
│   │   │   ├── sections.component.spec.ts
│   │   │   ├── sections.component.ts
│   │   │   ├── sections.module.ts
│   │   │   ├── tabs-section
│   │   │   │   ├── tabs-section.component.css
│   │   │   │   ├── tabs-section.component.html
│   │   │   │   ├── tabs-section.component.spec.ts
│   │   │   │   └── tabs-section.component.ts
│   │   │   ├── typography-section
│   │   │   │   ├── typography-section.component.css
│   │   │   │   ├── typography-section.component.html
│   │   │   │   ├── typography-section.component.spec.ts
│   │   │   │   └── typography-section.component.ts
│   │   │   └── versions-section
│   │   │       ├── versions-section.component.css
│   │   │       ├── versions-section.component.html
│   │   │       ├── versions-section.component.spec.ts
│   │   │       └── versions-section.component.ts
│   │   ├── shared
│   │   │   ├── footer
│   │   │   │   ├── footer.component.html
│   │   │   │   ├── footer.component.scss
│   │   │   │   ├── footer.component.spec.ts
│   │   │   │   └── footer.component.ts
│   │   │   └── navbar
│   │   │       ├── navbar.component.html
│   │   │       ├── navbar.component.scss
│   │   │       ├── navbar.component.spec.ts
│   │   │       └── navbar.component.ts
│   │   └── signup
│   │       ├── signup.component.html
│   │       ├── signup.component.scss
│   │       ├── signup.component.spec.ts
│   │       └── signup.component.ts
│   ├── assets
│   │   ├── css
│   │   ├── img
│   │   ├── js
│   │   ├── scss
│   │   │   ├── angular
│   │   │   ├── argon.scss
│   │   │   ├── bootstrap
│   │   │   └── custom
│   │   └── vendor
│   ├── browserslist
│   ├── environments
│   ├── favicon.ico
│   ├── index.html
│   ├── karma.conf.js
│   ├── main.ts
│   ├── polyfills.ts
│   ├── styles.css
│   ├── test.ts
│   ├── tsconfig.app.json
│   ├── tsconfig.spec.json
│   └── tslint.json
├── tsconfig.json
└── tslint.json

```


## Browser Support

At present, we officially aim to support the last two versions of the following browsers:

<img src="https://github.com/creativetimofficial/public-assets/blob/master/logos/chrome-logo.png?raw=true" width="64" height="64"> <img src="https://raw.githubusercontent.com/creativetimofficial/public-assets/master/logos/firefox-logo.png" width="64" height="64"> <img src="https://raw.githubusercontent.com/creativetimofficial/public-assets/master/logos/edge-logo.png" width="64" height="64"> <img src="https://raw.githubusercontent.com/creativetimofficial/public-assets/master/logos/safari-logo.png" width="64" height="64"> <img src="https://raw.githubusercontent.com/creativetimofficial/public-assets/master/logos/opera-logo.png" width="64" height="64">



## Resources
- Demo: <https://demos.creative-tim.com/victoria-Herschel-group/docs?ref=adsa-github-readme>
- Download Page: <https://www.creative-tim.com/product/victoria-Herschel-group?ref=adsa-github-readme>
- Documentation: <https://demos.creative-tim.com/victoria-Herschel-group/documentation/tutorial?ref=adsa-github-readme>
- License Agreement: <https://www.creative-tim.com/license?ref=adsa-github-readme>
- Support: <https://www.creative-tim.com/contact-us?ref=adsa-github-readme>
- Issues: [Github Issues Page](https://github.com/creativetimofficial/victoria-Herschel-group/issues?ref=adsa-github-readme)

## Reporting Issues

We use GitHub Issues as the official bug tracker for the Argon Design System Angular. Here are some advices for our users that want to report an issue:

1. Make sure that you are using the latest version of the Argon Design System Angular. Check the CHANGELOG from your kit on our [website](https://www.creative-tim.com/?ref=ada-github-readme).
2. Providing us reproducible steps for the issue will shorten the time it takes for it to be fixed.
3. Some issues may be browser specific, so specifying in what browser you encountered the issue might help.

## Licensing

- Copyright 2019 Creative Tim (https://www.creative-tim.com/?ref=adsa-github-readme)

- Licensed under MIT (https://github.com/creativetimofficial/victoria-Herschel-group/blob/master/LICENSE.md)

## Useful Links

- [Tutorials](https://www.youtube.com/channel/UCVyTG4sCw-rOvB9oHkzZD1w?ref=creativetim)
- [Affiliate Program](https://www.creative-tim.com/affiliates/new?ref=adsa-github-readme) (earn money)
- [Blog Creative Tim](http://blog.creative-tim.com/?ref=adsa-github-readme)
- [Free Products](https://www.creative-tim.com/bootstrap-themes/free?ref=adsa-github-readme) from Creative Tim
- [Premium Products](https://www.creative-tim.com/bootstrap-themes/premium?ref=adsa-github-readme) from Creative Tim
- [React Products](https://www.creative-tim.com/bootstrap-themes/react-themes?ref=adsa-github-readme) from Creative Tim
- [Angular Products](https://www.creative-tim.com/bootstrap-themes/angular-themes?ref=adsa-github-readme) from Creative Tim
- [VueJS Products](https://www.creative-tim.com/bootstrap-themes/vuejs-themes?ref=adsa-github-readme) from Creative Tim
- [More products](https://www.creative-tim.com/bootstrap-themes?ref=adsa-github-readme) from Creative Tim
- Check our Bundles [here](https://www.creative-tim.com/bundles?ref=adsa-github-readme)

### Social Media

Twitter: <https://twitter.com/CreativeTim?ref=creativetim>

Facebook: <https://www.facebook.com/CreativeTim?ref=creativetim>

Dribbble: <https://dribbble.com/creativetim?ref=creativetim>

Instagram: <https://www.instagram.com/CreativeTimOfficial?ref=creativetim>
